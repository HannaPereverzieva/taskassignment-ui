import { TaskGroup } from './taskGroup.model';
import { UserTask } from './UserTask.model';

export class TaskGroupDetails extends TaskGroup {
	userTasks!: UserTask[];
}
