import { TaskStatusEnum } from '../enums/taskStatus.enum';

export class UserTask {
	id!: number;
	name!: string;
	deadlineDate!: Date;
	status!: TaskStatusEnum;
	userId!: number;
	taskGroupId!: number;
}
