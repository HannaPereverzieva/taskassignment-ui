import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserTask } from '../models/UserTask.model';

@Injectable({
	providedIn: 'root',
})
export class TasksService {
	constructor(private http: HttpClient) {}

	public getTasksPerGroup(groupId: number): Observable<UserTask[]> {
		return this.http.get<UserTask[]>(`${environment.taApiUrl}/groups/${groupId}/tasks`);
	}

	public deleteTask(id: number): Observable<any> {
		return this.http.delete(`${environment.taApiUrl}/tasks/${id}`);
	}

	public addTask(task: UserTask): Observable<any> {
		return this.http.post(`${environment.taApiUrl}/tasks`, task);
	}

	public editTask(task: UserTask): Observable<any> {
		return this.http.patch(`${environment.taApiUrl}/tasks/${task.id}`, task);
	}
}
