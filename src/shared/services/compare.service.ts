import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class CompareService {
	constructor() {}

	public compareString(a: string, b: string, isAsc: boolean) {
		return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
	}

	public compareNumber(a: Number, b: Number, isAsc: boolean) {
		return a != null && b != null ? (a < b ? -1 : 1) * (isAsc ? 1 : -1) : 0;
	}
}
