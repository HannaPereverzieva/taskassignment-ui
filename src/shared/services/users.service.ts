import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

@Injectable({
	providedIn: 'root',
})
export class UsersService {
	public usersBehaviorSubject = new BehaviorSubject<User[]>([]);

	constructor(private http: HttpClient) {}

	public getUsers() {
		this.http.get<User[]>(`${environment.taApiUrl}/users`).subscribe((usersResponse: User[]) => {
			this.usersBehaviorSubject.next(usersResponse);
		});
	}
}
