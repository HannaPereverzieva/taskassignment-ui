import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class SpinnerService {
	visible = false;

	constructor() {}

	public show() {
		this.visible = true;
	}

	public hide() {
		this.visible = false;
	}
}
