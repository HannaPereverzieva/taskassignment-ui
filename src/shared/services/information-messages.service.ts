import { Injectable } from '@angular/core';
import { L10nService } from './l10n.service';
import { MatSnackBar } from '@angular/material';

@Injectable({
	providedIn: 'root',
})
export class InformationMessagesService {
	constructor(private snackBar: MatSnackBar, private l10nService: L10nService) {}

	public showCommonError() {
		this.l10nService.translate('messages', 'commonError').then(message => {
			this.showSnackBar(message);
		});
	}

	public showCommonSuccess() {
		this.l10nService.translate('messages', 'commonSuccess').then(message => {
			this.showSnackBar(message);
		});
	}

	private showSnackBar(message: string) {
		this.snackBar.open(message, '', { duration: 7000 });
	}
}
