import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
	providedIn: 'root',
})
export class L10nService {
	constructor(private translateService: TranslateService) {
		this.translateService.use('pl'); // only pl now
	}

	public translate(currentSection: string, key: string): Promise<string> {
		const result = new Promise<string>(resolve => {
			if (currentSection.length > 0 && key.length > 0) {
				this.translateService.get(currentSection + '.' + key).subscribe((translation: string) => {
					resolve(translation);
				});
			} else {
				resolve(currentSection + '.' + key);
			}
		});
		return result;
	}
}
