import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TaskGroup } from '../models/taskGroup.model';
import { TaskGroupDetails } from '../models/taskGroupDetails.model';

@Injectable({
	providedIn: 'root',
})
export class TaskGroupsService {
	public groupsBehaviorSubject = new BehaviorSubject<TaskGroup[]>([]);

	constructor(private http: HttpClient) {}

	public getTaskGroups(): void {
		this.http.get<TaskGroup[]>(`${environment.taApiUrl}/groups`).subscribe((usersResponse: TaskGroup[]) => {
			this.groupsBehaviorSubject.next(usersResponse);
		});
	}

	public getTaskGroup(id: number): Observable<TaskGroupDetails> {
		return this.http.get<TaskGroupDetails>(`${environment.taApiUrl}/groups/${id}`);
	}

	public deleteTaskGroup(id: number): Observable<any> {
		return this.http.delete(`${environment.taApiUrl}/groups/${id}`);
	}

	public addTaskGroup(group: TaskGroup): Observable<any> {
		return this.http.post(`${environment.taApiUrl}/groups`, group);
	}

	public editTaskGroup(group: TaskGroup): Observable<any> {
		return this.http.patch(`${environment.taApiUrl}/groups/${group.id}`, group);
	}
}
