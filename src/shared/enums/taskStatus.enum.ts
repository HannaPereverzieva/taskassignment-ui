export enum TaskStatusEnum {
	New = 1,
	InProgress = 2,
	Completed = 3,
}
