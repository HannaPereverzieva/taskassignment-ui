import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskGroupComponent } from './task-group/task-group.component';
import { TaskGroupEditComponent } from './task-group/task-group-edit/task-group-edit.component';

const routes: Routes = [
	{ path: '', component: TaskGroupComponent },
	{ path: 'create', component: TaskGroupEditComponent },
	{ path: 'edit/:groupId', component: TaskGroupEditComponent },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
