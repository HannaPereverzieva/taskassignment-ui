import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UsersService } from 'src/shared/services/users.service';
import { User } from 'src/shared/models/user.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserTask } from 'src/shared/models/UserTask.model';
import { MatTableDataSource } from '@angular/material';
import { InformationMessagesService } from 'src/shared/services/information-messages.service';
import { TasksService } from 'src/shared/services/tasks.service';
import { TaskStatusEnum } from 'src/shared/enums/taskStatus.enum';
import * as moment from 'moment';

@Component({
	selector: 'app-tasks-management',
	templateUrl: './tasks-management.component.html',
	styleUrls: ['./tasks-management.component.scss'],
})
export class TasksManagementComponent implements OnInit, OnDestroy {
	private usersSubscription!: Subscription;
	selectedTaskId!: number;
	taskForm!: FormGroup;
	users!: User[];
	displayedColumns: string[] = ['name', 'deadline', 'user', 'status', 'edit', 'remove'];
	dataSource = new MatTableDataSource<UserTask>();
	downloadingTasks = false;

	@Input() groupId: number;
	@Input() tasks: UserTask[];

	constructor(
		private tasksService: TasksService,
		private usersService: UsersService,
		private fb: FormBuilder,
		private informationMessagesService: InformationMessagesService
	) {
		this.initTaskForm();
	}

	ngOnInit() {
		this.subscribeLoadingUsers();
		this.usersService.getUsers();
		this.setDefaultValues();
		if (this.tasks) {
			this.dataSource.data = this.tasks;
		}
	}

	ngOnDestroy() {
		this.usersSubscription.unsubscribe();
	}

	subscribeLoadingUsers() {
		this.usersSubscription = this.usersService.usersBehaviorSubject.subscribe((users: User[]) => {
			this.users = users;
		});
	}

	getUserName(id: number) {
		if (this.users && id > 0) {
			const user = this.users.find(x => x.id === id);
			return user ? `${user.firstName} ${user.lastName}` : '';
		}
		return '';
	}

	onEditClick(selectedTask: UserTask) {
		this.selectedTaskId = selectedTask.id;
		this.taskForm.controls['taskName'].setValue(selectedTask.name);
		this.taskForm.controls['status'].setValue(selectedTask.status);
		this.taskForm.controls['deadline'].setValue(selectedTask.deadlineDate);
		this.taskForm.controls['user'].setValue(selectedTask.userId);
	}

	onSubmitClick() {
		if (!this.taskForm.valid) {
			return;
		}

		this.selectedTaskId ? this.editTask() : this.addTask();
	}

	clearForm() {
		this.taskForm.reset();
		this.taskForm.markAsUntouched();
		this.taskForm.markAsPristine();
		this.taskForm.updateValueAndValidity();
		this.setDefaultValues();
		this.selectedTaskId = null;
	}

	private setDefaultValues() {
		this.taskForm.controls['status'].setValue(TaskStatusEnum.New);
		this.taskForm.controls['user'].setValue(0);
	}

	private editTask() {
		const request = this.prepareRequest();
		request.id = this.selectedTaskId;
		this.tasksService.editTask(request).subscribe(
			response => {
				this.refreshData();
				this.selectedTaskId = null;
				this.informationMessagesService.showCommonSuccess();
			},
			error => {
				this.informationMessagesService.showCommonError();
			}
		);
	}

	private addTask() {
		const request = this.prepareRequest();
		this.tasksService.addTask(request).subscribe(
			response => {
				this.refreshData();
				this.informationMessagesService.showCommonSuccess();
			},
			error => {
				this.informationMessagesService.showCommonError();
			}
		);
	}

	onDeleteClick(task: UserTask) {
		this.tasksService.deleteTask(task.id).subscribe(
			_ => {
				this.getTasksPerGroup();
				this.informationMessagesService.showCommonSuccess();
			},
			_ => {
				this.informationMessagesService.showCommonError();
			}
		);
	}

	private getTasksPerGroup() {
		this.downloadingTasks = true;
		this.tasksService.getTasksPerGroup(this.groupId).subscribe(
			(tasks: UserTask[]) => {
				this.dataSource.data = tasks;
				this.downloadingTasks = false;
			},
			error => {
				this.informationMessagesService.showCommonError();
				this.downloadingTasks = false;
			}
		);
	}

	private prepareRequest() {
		const request = new UserTask();
		request.name = this.taskForm.value.taskName;
		request.status = this.taskForm.value.status;
		request.taskGroupId = this.groupId;
		const date = new Date(this.taskForm.value.deadline);
		date.setHours(12);
		request.deadlineDate = date;
		if (this.taskForm.value.user > 0) {
			request.userId = this.taskForm.value.user;
		}
		return request;
	}

	private refreshData() {
		this.getTasksPerGroup();
		this.clearForm();
	}

	private initTaskForm() {
		this.taskForm = this.fb.group({
			taskName: ['', Validators.required],
			deadline: ['', Validators.required],
			user: ['', Validators.required],
			status: ['', Validators.required],
		});
	}
}
