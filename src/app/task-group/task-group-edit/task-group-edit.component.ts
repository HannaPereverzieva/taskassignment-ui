import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TaskGroupsService } from 'src/shared/services/task-groups.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TaskGroup } from 'src/shared/models/taskGroup.model';
import { TaskGroupDetails } from 'src/shared/models/taskGroupDetails.model';
import { InformationMessagesService } from 'src/shared/services/information-messages.service';
import { SpinnerService } from 'src/shared/services/spinner.service';

@Component({
	selector: 'app-task-group-edit',
	templateUrl: './task-group-edit.component.html',
	styleUrls: ['./task-group-edit.component.scss'],
})
export class TaskGroupEditComponent implements OnInit {
	groupForm!: FormGroup;
	isEdit = false;
	selectedGroup!: TaskGroupDetails;

	constructor(
		private taskGroupsService: TaskGroupsService,
		private activatedRouter: ActivatedRoute,
		private fb: FormBuilder,
		private informationMessagesService: InformationMessagesService,
		private spinnerService: SpinnerService,
		public router: Router
	) {
		this.initGroupForm();
	}

	ngOnInit() {
		this.loadData();
	}

	loadData() {
		if (this.activatedRouter.paramMap) {
			this.activatedRouter.paramMap.subscribe(params => {
				const groupId = params.get('groupId');
				const selectedGroupId = parseInt(groupId, 10);
				if (selectedGroupId > 0) {
					this.spinnerService.show();
					this.taskGroupsService.getTaskGroup(parseInt(groupId, 10)).subscribe(group => {
						this.selectedGroup = group;
						this.groupForm.controls['groupName'].setValue(this.selectedGroup.name);
						this.isEdit = true;
						this.spinnerService.hide();
					});
				}
			});
		}
	}

	onSubmitClick() {
		if (!this.groupForm.valid) {
			return;
		}
		this.isEdit ? this.editGroup() : this.addGroup();
	}

	private addGroup() {
		const request = new TaskGroup();
		request.name = this.groupForm.value.groupName;
		this.taskGroupsService.addTaskGroup(request).subscribe(
			response => {
				this.selectedGroup = response as TaskGroupDetails;
				this.router.navigate(['/']);
				this.informationMessagesService.showCommonSuccess();
			},
			error => {
				this.informationMessagesService.showCommonError();
			}
		);
	}

	private editGroup() {
		const request = new TaskGroup();
		request.name = this.groupForm.value.groupName;
		request.id = this.selectedGroup.id;
		this.taskGroupsService.editTaskGroup(request).subscribe(
			response => {
				this.informationMessagesService.showCommonSuccess();
			},
			error => {
				this.informationMessagesService.showCommonError();
			}
		);
	}

	private initGroupForm() {
		this.groupForm = this.fb.group({
			groupName: ['', Validators.required],
		});
	}
}
