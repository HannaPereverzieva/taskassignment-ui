import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { TaskGroupsService } from 'src/shared/services/task-groups.service';
import { MatTableDataSource, MatSort, Sort, MatSnackBar } from '@angular/material';
import { TaskGroup } from 'src/shared/models/taskGroup.model';
import { CompareService } from 'src/shared/services/compare.service';
import { Router } from '@angular/router';
import { InformationMessagesService } from 'src/shared/services/information-messages.service';
import { Subscription } from 'rxjs';
import { SpinnerService } from 'src/shared/services/spinner.service';

@Component({
	selector: 'app-task-group',
	templateUrl: './task-group.component.html',
	styleUrls: ['./task-group.component.scss'],
})
export class TaskGroupComponent implements OnInit, OnDestroy {
	private groupsSubscription!: Subscription;

	@ViewChild(MatSort, { static: true }) sort!: MatSort;
	displayedColumns: string[] = ['name', 'count', 'edit', 'remove'];
	dataSource = new MatTableDataSource<TaskGroup>();
	rawSource = new Array<TaskGroup>();

	constructor(
		private taskGroupsService: TaskGroupsService,
		private compareService: CompareService,
		private router: Router,
		private informationMessagesService: InformationMessagesService,
		private spinnerService: SpinnerService
	) {}

	ngOnInit() {
		this.subscribeLoadingGroups();
		this.getTaskGroups();
	}

	ngOnDestroy() {
		this.groupsSubscription.unsubscribe();
	}

	onCreateClick() {
		this.router.navigate(['create']);
	}

	onEditClick(group: TaskGroup) {
		this.router.navigate([`edit/${group.id}`]);
	}

	onDeleteClick(group: TaskGroup) {
		this.taskGroupsService.deleteTaskGroup(group.id).subscribe(
			_ => {
				this.getTaskGroups();
				this.informationMessagesService.showCommonSuccess();
			},
			_ => {
				this.informationMessagesService.showCommonError();
			}
		);
	}

	sortData(sort: Sort) {
		this.dataSource.data = this.rawSource.slice();
		const isAsc = sort.direction === 'asc';
		if (sort.active && sort.direction !== '') {
			this.dataSource.data = this.dataSource.data.sort((a, b) => {
				switch (sort.active) {
					case 'name':
						return this.compareService.compareString(a.name, b.name, isAsc);
					case 'count':
						return this.compareService.compareNumber(a.tasksCount, b.tasksCount, isAsc);
					default:
						return 0;
				}
			});
		}
	}

	private getTaskGroups() {
		this.spinnerService.show();
		this.taskGroupsService.getTaskGroups();
	}

	private subscribeLoadingGroups() {
		this.groupsSubscription = this.taskGroupsService.groupsBehaviorSubject.subscribe((groups: TaskGroup[]) => {
			this.dataSource.data = groups;
			this.rawSource = groups;
			this.spinnerService.hide();
		});
	}
}
