import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaskGroupComponent } from './task-group/task-group.component';
import { TaskGroupEditComponent } from './task-group/task-group-edit/task-group-edit.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { OverlayModule } from '@angular/cdk/overlay';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {
	MatProgressSpinnerModule,
	MatCardModule,
	MatButtonModule,
	MatInputModule,
	MatFormFieldModule,
	MatTableModule,
	MatSortModule,
	MatSelectModule,
	MatIconModule,
	MatSnackBarModule,
	MatDatepickerModule,
	DateAdapter,
	MAT_DATE_FORMATS,
	MAT_DATE_LOCALE,
	MatNativeDateModule,
} from '@angular/material';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TasksManagementComponent } from './task-group/task-group-edit/tasks-management/tasks-management.component';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { SpinnerComponent } from 'src/shared/components/spinner/spinner.component';

@NgModule({
	declarations: [AppComponent, TaskGroupComponent, TaskGroupEditComponent, TasksManagementComponent, SpinnerComponent],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		HttpClientModule,
		MatProgressSpinnerModule,
		HttpClientModule,
		MatDatepickerModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient],
			},
		}),
		OverlayModule,

		MatCardModule,
		MatButtonModule,
		MatInputModule,
		MatFormFieldModule,
		MatTableModule,
		FormsModule,
		MatSortModule,
		MatSelectModule,
		ReactiveFormsModule,
		MatIconModule,
		MatSnackBarModule,
		MatNativeDateModule,
	],
	providers: [
		{ provide: MAT_DATE_LOCALE, useValue: 'pl-PL' },
		{ provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
		{
			provide: MAT_DATE_FORMATS,
			useValue: {
				display: {
					dateInput: 'DD/MM/YYYY',
					monthYearLabel: 'MMMM YYYY',
					dateA11yLabel: 'MM/DD/YYYY',
					monthYearA11yLabel: 'MMMM YYYY',
				},
			},
		},
	],
	bootstrap: [AppComponent],
})
export class AppModule {}

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http);
}
